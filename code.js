const puppeteer = require("puppeteer");
const merge = require("easy-pdf-merge");
var fs = require("fs");
const AWS = require("aws-sdk");
const { readdirSync, statSync } = require("fs");
const { join,resolve } = require("path");
let DIVIDER = 400;
var myArgs = process.argv.slice(2);
const resolvePath = resolve('./data/')
const resolvePathBase = resolve('./')

var BASE = `${resolvePath}/`;
var BASE_DIR = `${resolvePathBase}/`;
let schoolName = myArgs[0]

var s3 = new AWS.S3({ accessKeyId: "AKIAIEO7CE4ULCX3AKOQ", secretAccessKey: "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" });

const extractZip = require("extract-zip-promise")


AWS.config.update({
    accessKeyId: "AKIAIEO7CE4ULCX3AKOQ",
    secretAccessKey: "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",
    signatureVersion: "v4",
    region: "ap-south-1"
});
  


(async () => {
  const WAITING = await extractZip(BASE_DIR+`${schoolName}.zip`, {dir: BASE}, function (err) {
      err?console.log('SUCCESSFULLY UN ZIPPED'):console.log('ERROR IN UN ZIPPING')
  })
  let MAPPING = {};
  const items = fs.readdirSync(BASE);
  for (var i = 0; i < items.length; i++) {
    const data = fs.lstatSync(BASE + items[i]).isDirectory();
    if (data) {
      let URL = BASE + items[i];
      console.log(URL);
      const subItems = fs.readdirSync(URL);
      let classID = items[i];
      var pdfUrls = [];
      //console.log(subItems.length);
        for (var j = 0; j < subItems.length; j++) {
        if (subItems[j].includes(".html")) {
        pdfUrls.push({url:"file://" + URL +'/'+ subItems[j],name:subItems[j]});
        }

     }
     console.log(pdfUrls.length)
     const PDF = await genPDF(pdfUrls,classID);
    }
  }
})();


async function genPDF (pdfUrls,classID){
    let asyncCID = classID;
    const browser = await puppeteer.launch({ headless: true });
    const page = await browser.newPage();
    var pdfFiles = [];
    let j = 0;
    for (var i = 0; i < pdfUrls.length; i++) {
        //console.log(pdfUrls[i].name)
      await page.goto(pdfUrls[i].url, { waitUntil: "networkidle0" });
      var pdfFileName = `./GEN/${pdfUrls[i].name.slice(0,-5)}_class_${asyncCID}_`+ (i + 1) + ".pdf";
      pdfFiles.push(pdfFileName);
      await page.pdf({
        printBackground: true,
        scale: 1,
        path: pdfFileName,
        format: "A5"
      });
      //console.log(`GENERATED :: class_${classID}_`+ (i + 1) + ".pdf")
      if ((i + 1) % DIVIDER == 0) {
        await mergeMultiplePDF(pdfFiles, j,asyncCID);
        for (var p = 0; p < pdfFiles.length; p++) {
          fs.unlinkSync(BASE_DIR + pdfFiles[p]);
        }
        j++;
        pdfFiles = [];
      } else if (pdfUrls.length - (i + 1) < DIVIDER && i + 1 == pdfUrls.length) {
        await mergeMultiplePDF(pdfFiles, j,asyncCID);
        for (var p = 0; p < pdfFiles.length; p++) {
          fs.unlinkSync(BASE_DIR + pdfFiles[p]);
        }
        j++;
        pdfFiles = [];
      }
    }
   
    await browser.close();
  };



const mergeMultiplePDF = (pdfFiles, j,classID) => {
    
  return new Promise((resolve, reject) => {
    merge(pdfFiles, `./COMB/${classID}_FINAL_${j}_.pdf`, function(err) {
      if (err) {
        console.log(err);
        reject(err);
      }
      console.log(`CLASS GENERATED ====> ${classID}`)
      uploadFile(BASE_DIR+`COMB/${classID}_FINAL_${j}_.pdf`,'test-report-html-png',`${schoolName}_${classID}_FINAL_${j}_.pdf`,classID)
     // console.log("MERGED "+ `${classID}_FINAL_`+ j + "_.pdf");
      resolve();
    });
  });
};


const uploadFile = async (filePath, bucketName, key,classID) => {
    fs.readFile(filePath, async (err, data) => {
    if (err) console.error(err);
    var base64data = new Buffer(data, 'binary');
    var params = {
      Bucket: bucketName,
      Key: `user_reports/${schoolName}/${classID}/`+ key,
      contentType : 'application/pdf',
      Body: base64data,
      ACL: 'public-read-write'
    };
    s3.upload(params, (err, data) => {
      if (err) console.error(`Upload Error ${err}`);
       console.log(`CLASS UPLOADED ====> ${classID}`)
      //uploaded--;
    });
  });
};
